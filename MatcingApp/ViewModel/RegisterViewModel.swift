//
//  RegisterViewModel.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/12/14.
//

import Foundation
import RxSwift
import RxCocoa

// 可読性が上がる
protocol RegisterViewModelInputs {
    var nameTextInput: AnyObserver<String> { get }
    var emailTextInput: AnyObserver<String> { get }
    var passwordTextInput: AnyObserver<String> { get }
}

protocol RegisterViewModelOutPuts {
    var nameTextOutput: PublishSubject<String> { get }
    var emailTextOutput: PublishSubject<String> { get }
    var passwordTextOutput: PublishSubject<String> { get }
}

class RegisterViewModel: RegisterViewModelInputs, RegisterViewModelOutPuts {
    
    private let disposeBag = DisposeBag()
    
    // MARK: observable
    // RegisterViewModelから出ていく情報
    var nameTextOutput = PublishSubject<String>()
    var emailTextOutput = PublishSubject<String>()
    var passwordTextOutput = PublishSubject<String>()
    var validRegisterSubject = BehaviorSubject<Bool>(value: false) //今の状態を反映した上で変化があったらそれを反映する
    
    // MARK: observer
     // ViewControllerから入ってくる情報
    var nameTextInput: AnyObserver<String> {
        nameTextOutput.asObserver()
    }

    var emailTextInput: AnyObserver<String> {
        emailTextOutput.asObserver()
    }

    var passwordTextInput: AnyObserver<String> {
        passwordTextOutput.asObserver()
    }
    
    // Driverに変換する値を持たずCompletedは発生しない
    var validRegisterDriver: Driver<Bool> = Driver.never()
    
    init() {
        
        // vaildALLがここで流れる？？
        validRegisterDriver = validRegisterSubject
            .asDriver(onErrorDriveWith: Driver.empty())
        
        // trueかfalseかを代入
        let nameValid = nameTextOutput
            .asObservable()
            .map { text -> Bool in
                // 5文字以上の場合はtrueを返す
                return text.count >= 5
            }
        
        let emailVaild = emailTextOutput
            .asObservable()
            .map { text -> Bool in
                return text.count >= 5
            }
        
        let passwordlVaild = passwordTextOutput
            .asObservable()
            .map { text -> Bool in
                return text.count >= 5
            }
        
        // 3つの値を監視
        Observable.combineLatest(nameValid, emailVaild, passwordlVaild) {$0 && $1 && $2}
        // 全てtrueの場合vaildALL
        .subscribe { vaildALL in
            // validRegisterDriverをここで流す
            self.validRegisterSubject.onNext(vaildALL)
        }
        .disposed(by: disposeBag)

    }
    
}
