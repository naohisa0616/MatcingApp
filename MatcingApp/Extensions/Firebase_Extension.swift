//
//  Firebase_Extension.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/12/24.
//

import Firebase
import UIKit

// MARK: - Auth
extension Auth {
    
    static func createUserToFireAuth(email: String?, password: String?, name: String?, completion: @escaping (Bool) -> Void) {
        guard let email = email else {
            return
        }
        guard let password = password else {
            return
        }
        //emailとpasswordを保存
        Auth.auth().createUser(withEmail: email, password: password) { (auth, err) in
            if let err = err {
                print("auth情報の保存に失敗: ", err)
                return
            }
            
            //uidとはユーザー独自のID
            guard let uid = auth?.user.uid else {
                return
            }
            Firestore.setUserDataToFirestore(email: email,uid: uid, name: name) { success in
                
                completion(success)
            }
        }
    }
    
    static func loginWithFireAuth(email: String, password: String, completion: @escaping (Bool) -> Void) {
        
        Auth.auth().signIn(withEmail: email, password: password) { res, err in
            if let err = err {
                print("ログインに失敗: ", err)
                completion(false)
                return
            }
            
            print("ログインに成功")
            completion(true)
        }
    }
    
}

// MARK: - Firestore
extension Firestore {
    
    static func setUserDataToFirestore(email: String, uid: String, name: String?, completion: @escaping (Bool) -> ()) {
        guard let name = name else { return }
        let document = [
            "name" : name,
            "email" : email,
            "createdAt" : Timestamp()
        ] as [String : Any]
        
        Firestore.firestore().collection("users").document(uid).setData(document) { err in
            
            if let err = err {
                print("ユーザー情報の保存に失敗: ", err)
                return
            }
            
            // 処理に成功したか分かるように、setUserDataToFirestoreにコールバックを設定
            completion(true)
            print("ユーザー情報の保存に成功")
        }
    }
    
    // Firestoreからユーザー情報を取得
    static func fetchUserFromFirestore(uid: String, completion: @escaping (User?) -> Void) {
        
        // 値の更新があった際に、自動的に更新
        Firestore.firestore().collection("users").document(uid).addSnapshotListener { (snapshot, err) in
            if let err = err {
                print("ユーザー情報の取得に失敗: ", err)
                completion(nil)
                return
            }
            
            // snapshotでユーザーの情報を取得
            guard let dic = snapshot?.data() else { return }
            // Userの型に変換
            let user = User(dic: dic)
            completion(user)
        }
    }
    
    //Firestoreから自分以外のユーザー情報を取得
    static func fetchUsersFromFirestore(completion: @escaping ([User]) -> Void) {
        Firestore.firestore().collection("users").getDocuments { (snapshots, err) in
            if let err = err {
                print("ユーザー情報の取得に失敗: ", err)
                return
            }
            
            // 変数の宣言とappendの処理が要らなくなる
            let users = snapshots?.documents.map({ (snapshot) -> User in
                let dic = snapshot.data()
                let user = User(dic: dic)
                return user
            })
            
            completion(users ?? [User]())
        }
    }
    
    // ユーザー情報の更新
    static func updateUserInfo (dic: [String: Any], compeltion: @escaping () -> Void) {
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().collection("users").document(uid).updateData(dic) { err in
            if let err = err {
                print("ユーザー情報の更新に失敗: ", err)
                return
            }
            
            compeltion()
            print("ユーザー情報の更新に成功")
        }
    }
    
}

// MARK: - Storage
extension Storage {
    
    // ユーザーの情報をFireStorageに保存
    static func addProfileImageToStorage(image: UIImage, dic: [String: Any], completion: @escaping () -> Void) {
        
        // 画像をJPEG圧縮（0.3は18KB）
        guard let uploadImage = image.jpegData(compressionQuality: 0.3) else { return }
        
        let filename = NSUUID().uuidString
        
        // ルート参照を指す、profile_imageの中のfilenameに画像が保存
        let storageRef = Storage.storage().reference().child("profile_image").child(filename)
        
        // Cloud Storage にファイルをアップロード（画像を保存）
        storageRef.putData(uploadImage, metadata: nil) { metadata, error in
            if let err = error {
                print("画像の保存に失敗しました。:", err)
                return
            }
            
            // 保存した情報をダウンロード
            storageRef.downloadURL { url, error in
                if let err = error {
                    print("画像の取得に失敗しました。:", err)
                    return
                }
                
                guard let urlString = url?.absoluteString else { return }
                var dicWithImage = dic
                dicWithImage["profileImageUrl"] = urlString
                
                Firestore.updateUserInfo(dic: dicWithImage) {
                    completion()
                }
            }
        }
    }
    
}
