//
//  CardImageView.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/11/16.
//

import UIKit

class CardImageView:UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .gray
        //角を丸くする。
        layer.cornerRadius = 10
        //contentMode（View のサイズが変わる時にコンテンツをどう表示するかを決めるもの）
        //View のサイズと画像のサイズが同じ
        contentMode = .scaleToFill
        contentMode = .scaleAspectFill
//        image = UIImage(named: "test-image")
        clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
