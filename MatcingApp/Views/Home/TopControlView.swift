//
//  TopControlView.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/09/07.
//

import UIKit
import RxCocoa
import RxSwift

class TopControlView: UIView {
    
    //disposeBagは、disposeBagオブジェクト自身が開放されるタイミングで、登録されたsubscriptionをdispose
    private let disposeBag = DisposeBag()
    
    let tinderButton = createTopButton(imageName: "tinder_selected", unselectedImage: "tinder_unselected")
    let goodButton = createTopButton(imageName: "good_selected", unselectedImage: "good_unselected")
    let commentButton = createTopButton(imageName: "comment_selected", unselectedImage: "comment_unselected")
    let profileButton = createTopButton(imageName: "profile_selected", unselectedImage: "profile_unselected")
    
    
    static private func createTopButton(imageName: String, unselectedImage: String) -> UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: imageName), for: .selected) //選択時のボタン
        button.setImage(UIImage(named: unselectedImage), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit //縦横の比率はそのままで長い辺を基準に全体を表示する
        return button
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupLayout()
        setupBindings()

    }
    
    private func setupLayout() {
        let baseStackView = UIStackView(arrangedSubviews: [tinderButton, goodButton, commentButton, profileButton])
        baseStackView.axis = .horizontal
        baseStackView.distribution = .fillEqually
        baseStackView.spacing = 43
        baseStackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(baseStackView)
        
        baseStackView.anchor(top: topAnchor, bottom: bottomAnchor, left: leftAnchor, right: rightAnchor, leftPadding: 40, rightPadding: 40)
        
        tinderButton.isSelected = true
    
    }
    
    //ボタンの処理
    private func setupBindings() {
        
        tinderButton.rx.tap
            .asDriver() //メインスレッドで通知、shareReplayLatestWhileConnected を使った Cold-Hot 変換、エラー処理
            .drive(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.handleSelectedButton(selectedButton: self.tinderButton)
            })
            .disposed(by: disposeBag) //Viewが終わると同時に破棄する

        goodButton.rx.tap
            .asDriver()
            .drive(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.handleSelectedButton(selectedButton: self.goodButton)
            })
            .disposed(by: disposeBag)
        
        commentButton.rx.tap
            .asDriver()
            .drive(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.handleSelectedButton(selectedButton: self.commentButton)
            })
            .disposed(by: disposeBag)
        
        profileButton.rx.tap
            .asDriver()
            .drive(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.handleSelectedButton(selectedButton: self.profileButton)
            })
            .disposed(by: disposeBag)
        
    }
    
    private func handleSelectedButton(selectedButton: UIButton) {
        let buttons = [tinderButton, goodButton, commentButton, profileButton]
        
        buttons.forEach { button in
            if button == selectedButton {
                button.isSelected = true //button.setImage(UIImage(named: imageName), for: .selected)ここの処理が呼ばれる
            } else {
                button.isSelected = false
            }
            
        }
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
