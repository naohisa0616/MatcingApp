//
//  CardView.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/10/11.
//

import UIKit
import SDWebImage

class CardView: UIView {
    
    //グラデーションの色や方向を指定
    private let gradientLayer = CAGradientLayer()
    
    // MARK: UIViews
    private let cardImageView = CardImageView(frame: .zero)
    
    private let nameLabel = CardInfoLabel(text: "Taro, 22", font: .systemFont(ofSize: 40, weight: .heavy))
    
    private let infoButton = UIButton(type: .system).createCardInfoButton()
    
    private let residenceLabel = CardInfoLabel(text: "日本、千葉", font: .systemFont(ofSize: 20, weight: .regular))
    
    private let hobbyLabel = CardInfoLabel(text: "筋トレ", font: .systemFont(ofSize: 25, weight: .regular))
    
    private let introductionLabel = CardInfoLabel(text: "体を鍛えるのが好きです。", font: .systemFont(ofSize: 25, weight: .regular))
    
    private let goodLabel = CardInfoLabel(text: "GOOD", textColor: .rgb(red: 137, green: 223, blue: 86))
    
    private let nopeLabel = CardInfoLabel(text: "NOPE", textColor: .rgb(red: 222, green: 110, blue: 110))
    
    init(user: User) {
        super.init(frame: .zero)
        
        setupLayout(user: user)
        setupGradientlayer()
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panCardView))
        self.addGestureRecognizer(panGesture)

    }
    
    private func setupGradientlayer() {
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.3, 1.1]
        cardImageView.layer.addSublayer(gradientLayer)
    }
    
    //ビューが作成されて表示される前、ビューのサイズが変更されて画面が更新される前に呼ばれる。
    override func layoutSubviews() {
        //コメントアウトするとグラデーションが反映されない。
        gradientLayer.frame = self.bounds
    }
    
    @objc private func panCardView(gesture: UIPanGestureRecognizer) {
        //指定されたビューの座標系での移動
        let translation = gesture.translation(in: self)
        guard let view = gesture.view else { return }
        
        if gesture.state == .changed {

            self.handlePanChange(translation: translation)
            
        } else if gesture.state == .ended {
            self.handlePanEnded(view: view,translation: translation)
        }
    }
    
    private func handlePanChange(translation: CGPoint) {
        let degree: CGFloat = translation.x / 20
        //円周率を用いて回転を加える
        let angle = degree * .pi / 100
        
        let rotateTranslation = CGAffineTransform(rotationAngle: angle)
        //オブジェクトが描かれている場所を移動させる
        self.transform = rotateTranslation.translatedBy(x: translation.x, y: translation.y)
        
        let ratio: CGFloat = 1 / 100
        let ratioValue = ratio * translation.x
        
        if translation.x > 0 {
            self.goodLabel.alpha = ratioValue
        } else if translation.x < 0 {
            self.nopeLabel.alpha = -ratioValue
        }
        print("translation.x: ", translation.x)
    }
    
    private func handlePanEnded(view: UIView, translation: CGPoint) {
        // カードビューが左に動かした際の制御
        if translation.x <= -120 {
            view.removeCardViewAnimation(x: -600)
        } else if translation.x >= 120 {
            view.removeCardViewAnimation(x: 600)
        } else {
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: []) {
                //オブジェクトを元に戻す
                self.transform = .identity
                //アニメーション認識
                self.layoutIfNeeded()
                self.goodLabel.alpha = 0
                self.nopeLabel.alpha = 0
            }
        }
        
    }
    
    private func setupLayout(user: User) {
        let infoVerticalStackView = UIStackView(arrangedSubviews: [residenceLabel, hobbyLabel, introductionLabel])
        infoVerticalStackView.axis = .vertical
        
        let baseStackView = UIStackView(arrangedSubviews: [infoVerticalStackView, infoButton])
        baseStackView.axis = .horizontal
        
        // Viewの配置を作成
        addSubview(cardImageView)
        addSubview(nameLabel)
        addSubview(baseStackView)
        addSubview(goodLabel)
        addSubview(nopeLabel)
        cardImageView.anchor(top: topAnchor, bottom: bottomAnchor, left:  leftAnchor, right: rightAnchor, leftPadding: 10, rightPadding: 10)
        infoButton.anchor(width: 40)
        baseStackView.anchor(bottom: cardImageView.bottomAnchor, left: cardImageView.leftAnchor, right: cardImageView.rightAnchor, bottomPadding: 20, leftPadding: 20, rightPadding: 20)
        nameLabel.anchor(bottom: baseStackView.topAnchor, left: cardImageView.leftAnchor, bottomPadding: 10, leftPadding: 20)
        goodLabel.anchor(top: cardImageView.topAnchor, left: cardImageView.leftAnchor, width: 140, height: 55, topPadding: 25, leftPadding: 20)
        nopeLabel.anchor(top: cardImageView.topAnchor, right: cardImageView.rightAnchor, width: 140, height: 55, topPadding: 25, rightPadding: 20)
        // ユーザー情報をViewに反映
        nameLabel.text = user.name
        introductionLabel.text = user.email
        
        if let url = URL(string: user.profielImageUrl) {
            cardImageView.sd_setImage(with: url)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
