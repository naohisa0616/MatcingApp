//
//  RegisterTextField.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/12/10.
//

import UIKit

class RegisterTextField: UITextField {
    
    init(placeHolder: String) {
        super.init(frame: .zero)
        placeholder = placeHolder
        //角丸
        borderStyle = .roundedRect
        font = .systemFont(ofSize: 14)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
