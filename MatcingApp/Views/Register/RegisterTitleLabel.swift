//
//  RegisterTitleLabel.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/12/11.
//

import UIKit

class RegisterTitleLabel: UILabel {

    init(text: String) {
        super.init(frame: .zero)
        self.text = text
        self.font = .boldSystemFont(ofSize: 80) //指定されたサイズの太字
        self.textColor = .white

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
