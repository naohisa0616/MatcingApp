//
//  ProfileImageView.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2022/01/11.
//

import UIKit

class ProfileImageView: UIImageView {
    
    
    init() {
        super.init(frame: .zero)
        
        self.image = UIImage(named: "no_image")
        // 縦横の比率はそのままで短い辺を基準に全体を表示する
        self.contentMode = .scaleAspectFill
        self.layer.cornerRadius = 90
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
