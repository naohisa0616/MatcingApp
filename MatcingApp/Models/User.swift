//
//  User.swift
//  MatcingApp
//
//  Created by 宮崎直久 on 2021/12/29.
//

import Foundation
import Firebase

// 取得したユーザー情報を辞書型から変換（使いやすくするため）
class User {
    
    var email: String
    var name: String
    var age: String
    var residence: String
    var hobby: String
    var introduction: String
    var createdAt: Timestamp
    var profielImageUrl: String
        
    init(dic: [String: Any]) {
        
        // dictionaryのキーを指定
        self.email = dic["email"] as? String ?? ""
        self.name = dic["name"] as? String ?? ""
        self.age = dic["age"] as? String ?? ""
        self.residence = dic["residence"] as? String ?? ""
        self.hobby = dic["hobby"] as? String ?? ""
        self.introduction = dic["introduction"] as? String ?? ""
        self.createdAt = dic["createdAt"] as? Timestamp ?? Timestamp()
        self.profielImageUrl = dic["profielImageUrl"] as? String ?? ""
    }
    
    
}
